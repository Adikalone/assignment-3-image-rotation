#include "../include/util.h"

enum open_status open_file(FILE* in, char* name, char* mode){
  in = fopen(name, mode);
  if (in == NULL){
    return OPEN_ERROR;
  }
  return OPEN_OK;
}

FILE* open_file_for_read(enum open_status* open_status, char* fname){
  FILE* in = fopen(fname, "r+");
  if (in == NULL){
    *open_status = OPEN_ERROR;
    return NULL;
  }
  *open_status = OPEN_OK;
  return in;
}

FILE* open_file_for_write(enum open_status* open_status, char* fname){
  FILE* in = fopen(fname, "w+");
  if (in == NULL){
    *open_status = OPEN_ERROR;
    return NULL;
  }
  *open_status = OPEN_OK;
  return in;
}


enum close_status close_file(FILE *in){
  fclose(in);
  return CLOSE_OK;
}
