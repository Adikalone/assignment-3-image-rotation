#include "../include/transform_rotate.h"
#include <stdint.h>

void rotate_90(struct image const source, struct image* new){
//  struct image* new_image = new_image(source.height, source.width);

//  new_image->width = source.height;
//  new_image->height = source.width;
  new->width = source.height;
  new->height = source.width;

 // uint64_t center = (new->width + new->height) / 2;

  for (uint64_t i = 0; i < new->height; i++){
    for (uint64_t j = 0; j < new->width; j++){ 
//      struct pixel pixel = new_image->data[i];
        //new->data[i * new->width + j] = source.data[j * source.width + (source.width - i - 1)];
        new->data[i * new->width + j] = source.data[(source.height - j - 1) * source.width + i];
     }
  }
  //return new;
}

void rotate_0(struct image const source, struct image* new){
  new->height = source.height;
  new->width = source.width;
  for(uint64_t i = 0; i < source.height; i++){
    for(uint64_t j = 0; j < source.width; j++){
      new->data[i * source.width + j] = source.data[i * source.width + j];
    }
  }
   // new->data = source.data;
  //return new;
}


void rotate_180(struct image const source, struct image* new){
 // struct image* old = 
  struct image* old = create_empty_image();
  get_memmory_for_data(old, source.width, source.height);
  rotate_90(source, new);
  rotate_0(*new, old);
  rotate_90(*old, new);
 // return new;
  free_image(old);
}

void rotate_270(struct image const source, struct image* new){
  struct image* old = create_empty_image();
  get_memmory_for_data(old, source.width, source.height);

  rotate_90(source, new);
  rotate_0(*new, old);
  rotate_90(*old, new);
  rotate_0(*new, old);
  rotate_90(*old, new);
  //new = rotate_90(source, new);
  //new = rotate_90(source, new);
  //new = rotate_90(source, new);
  //return new;
  free_image(old);
}


struct image* rotate(struct image const source, int16_t angel){
//  struct image* new_img = new_image(source.width, source.height);
  struct image* new_img = create_empty_image();
  get_memmory_for_data(new_img, source.width, source.height);
  if(angel == 0){
    rotate_0(source, new_img);
    return new_img;
  }
  if(angel == 90){
    //rotate_90(source, new_img);
    rotate_270(source, new_img);
    return new_img;
  }
  if(angel == 180){
    rotate_180(source, new_img);
    return new_img;
  }
  if(angel == 270){
    rotate_90(source, new_img);
    //rotate_270(source, new_img);
    return new_img;
  }
  if(angel == -90){
    rotate_90(source, new_img);
    //rotate_270(source, new_img);
    return new_img;
  }
  if(angel == -180){
    rotate_180(source, new_img);
    return new_img;
  }
  if(angel == -270){
    //rotate_90(source, new_img);
    rotate_270(source, new_img);
    return new_img;
  }
  return new_img;
}

