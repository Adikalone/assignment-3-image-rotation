#include "../include/header_bmp.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/cdefs.h>

struct bmp_header* create_headers(void){
  struct bmp_header* headers = malloc(sizeof(uint16_t)* 3 + sizeof(uint32_t)*12);
  return headers;
}

void free_header(struct bmp_header* bmp_header){
  if(bmp_header){
    free(bmp_header);
  }
}

enum read_status from_bmp(FILE *in, struct image* img){
//  struct bmp_header* headers = create_headers();
  struct bmp_header* headers = create_headers();

  fread(headers, sizeof(struct bmp_header), 1, in);

  if (headers->bfType != 19778){
    free_header(headers);
    return READ_INVALID_TYPE;
  }
  if (headers->biBitCount != 24){
    free_header(headers);
    return READ_INVALID_BITS;
  }

  img->width = headers->biWidth;
  img->height = headers->biHeight;

  get_memmory_for_data(img, headers->biWidth, headers->biHeight);


  uint8_t bytesPerPixel = headers->biBitCount/8;
  //size one row in bytes
  uint32_t rowSize = bytesPerPixel * headers->biWidth;
  uint32_t rowPadding = (4 - (rowSize % 4)) % 4;
  //count of written row in memory
 // uint8_t rowsWritten = 0;
  //one row reading from file
  unsigned char* row = malloc(rowSize + rowPadding);
//  struct pixel* p = &img->data[(headers->biHeight - 1) * rowSize];
//  struct pixel* p = &img->data[0];
//  unsigned char* p = (unsigned char*)img->data;

  fseek(in, headers->bOffBits, SEEK_SET);

//  uint64_t count = 0;

  for(uint32_t k = 0; k < headers->biHeight; k++){
 //   printf("in cickle");
    fread(row, rowSize + rowPadding, 1, in);
 //   if (count > (rowSize + rowPadding)){
 //     free_header(headers);
 //     free(row);
 //     return READ_INVALID_BITS;
 //   }
    for(uint32_t i = 0; i < rowSize; i += bytesPerPixel){
//      *p = row[i]; p++;
      img->data[(k * headers->biWidth) + i/bytesPerPixel].b = row[i];
      img->data[(k * headers->biWidth) + i/bytesPerPixel].g = row[i + 1];
      img->data[(k * headers->biWidth) + i/bytesPerPixel].r = row[i + 2];
    }
//    rowsWritten++;
//    p = p - 2 * rowSize;
  }
//  printf("go out");

  free(row);
  free_header(headers);
  return READ_OK;
}

enum write_status to_bmp(FILE *out, struct image const* img){
  struct bmp_header* headers = create_headers();
  
  headers->bfType = 19778;
 // headers->bfileSize = ;
  headers->bfReserved = 0;
  headers->bOffBits = 54;
  headers->biSize = 40;
  headers->biWidth = img->width;
  headers->biHeight = img->height;
  headers->biPlanes = 1;
  headers->biBitCount = 24;
  headers->biCompression = 0;
  headers->biSizeImage = sizeof(struct pixel) * img->width * img->height ;
  headers->biXPelsPerMeter = img->width;
  headers->biYPelsPerMeter = img->height;
  headers->biClrUsed = 0;
  headers->biClrImportant = 0;
  headers->bfileSize = headers->biSizeImage + headers->bOffBits;
  
  fwrite(headers, sizeof(struct bmp_header), 1, out);

  uint8_t bytesPerPixel = headers->biBitCount/8;
  //size one row in bytes
  uint32_t rowSize = bytesPerPixel * img->width;
  uint32_t rowPadding = (4 - (rowSize % 4)) % 4;
  //count of written row in memory
//  uint8_t rowsWritten = 0;
  //one row reading from file
  unsigned char* row = malloc(rowSize + rowPadding);

//  struct pixel* p = &img->data[(headers->biHeight - 1) * rowSize];
//  struct pixel* p = img->data;

  fseek(out, headers->bOffBits, SEEK_SET);

  for(uint32_t countRow = 0; countRow < headers->biHeight; countRow++){
    for(uint32_t i = 0; i < rowSize; i+= bytesPerPixel){
      row[i] = img->data[(countRow * img->width) + i/bytesPerPixel].b;
      row[i+1] = img->data[(countRow * img->width) + i/bytesPerPixel].g;
      row[i+2] = img->data[(countRow * img->width) + i/bytesPerPixel].r;
    }
    for(uint32_t i = rowSize; i < rowSize + rowPadding; i++){
      row[i] = 0; 
    }
    fwrite(row, rowSize + rowPadding, 1, out);
  }
//    for(int i = rowSize; i < rowSize + rowPadding; i++){
//      struct pixel pix = {0, 0, 0};
//      row[i] = ;
//    }
//    fwrite(row, rowSize + rowPadding, 1, out);
//    rowsWritten++;
//    p = p - 2 * rowSize;
//  }
  free(row);

  free_header(headers);

  return WRITE_OK;
}
