#include "../include/header_bmp.h"
#include "../include/image_struct.h"
#include "../include/transform_rotate.h"
#include "../include/util.h"
#include <malloc.h>
#include <stdlib.h>



int main( int argc, char** argv ) {
  (void) argc; (void) argv; // supress 'unused parameters' warning
  if(argc < 4){
    printf("Not enought arguments");
    return 0;
  }
  enum open_status open_status = 0;
  enum read_status read_status = 0;
  enum write_status write_status = 0;
  enum close_status close_status = 0;
  char* fname = argv[1];
  char* fnew_name = argv[2];
 // int16_t angel = int(argv[2]);
  int16_t angle = 0;
  angle = (short)atoi(argv[3]);
//  FILE* in = NULL;
//  FILE* out = NULL;

//  char* mode_read = malloc(2);
//  *mode_read = 'r';
//  char* mode_write = malloc(2);
//  *mode_write = 'w';

  //open file
  FILE* in = open_file_for_read(&open_status, fname); 
  if (open_status != OPEN_OK){
    printf("Open error %d", open_status);
    close_file(in);
    return 0;
  }
  struct image* img = create_empty_image();

  read_status = from_bmp(in, img);
  if(read_status != READ_OK){
    printf("Read error %d", read_status);
    free_image(img);
    close_file(in);
    return 0;
  }

  close_status = close_file(in);
  if(close_status != CLOSE_OK){
    printf("Close error %d", close_status);
    return 0;
  }


  struct image* new = rotate(*img, angle);
  free_image(img);
  
  FILE* out = open_file_for_write(&open_status, fnew_name);
  if (open_status != OPEN_OK){
    printf("Open error %d", open_status);
    //free_image(img);
    free_image(new);
    close_file(out);
    return 0;
  }
  //free_image(&new);
  write_status = to_bmp(out, new);
  free_image(new);
 // free_image(img);
  if(write_status != WRITE_OK){
    printf("Write error %d", write_status);
    return 0;
  }
  close_status = close_file(out);
  if(close_status != CLOSE_OK){
    printf("Close error %d", close_status);
    return 0;
  }

//  free(mode_write);
//  free(mode_read);
  return 0;
}
