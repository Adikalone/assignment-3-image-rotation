//#include "image_struct.h"
#include "../include/image_struct.h"
#include <malloc.h>
#include <stdint.h>


struct image* create_empty_image(void){
  struct image* image = malloc(sizeof(uint64_t) * 2 + sizeof(struct pixel*));
  image->data = NULL;
  return image;
}

void get_memmory_for_data(struct image* img, const uint64_t width, const uint64_t height){
  struct pixel* data = malloc(width * height * sizeof(struct pixel));
  img->data = data;
}

struct image* new_image(const uint64_t width, const uint64_t height){
//  struct pixel* data = malloc(width * height * sizeof(uint8_t));
//  struct image* image = malloc(sizeof(uint64_t) * 2);
//  image->data = data;
  struct image* image = create_empty_image();
  get_memmory_for_data(image, width, height);
  return image;
}


void free_image(struct image* image){ 
  if(image->data){
    free(image->data);
  }
  if(image){
    free(image);
  }
}

void free_empty_image(struct image* image){
  free(image);
}
