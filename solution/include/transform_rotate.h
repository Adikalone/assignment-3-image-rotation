#ifndef ROTATE
#define ROTATE
#include "image_struct.h"

struct image* rotate(struct image const source, int16_t angel);

#endif
