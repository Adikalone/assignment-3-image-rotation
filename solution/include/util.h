#ifndef IO
#include <stdio.h>
#endif // !DEBUG

#ifndef UTIL
#define UTIL

enum open_status {
  OPEN_OK = 0,
  OPEN_ERROR
};

enum open_status open_file(FILE* in, char* name, char* mode);

enum close_status {
  CLOSE_OK = 0,
  CLOSE_ERROR
};


FILE* open_file_for_read(enum open_status* open_status, char* fname);
FILE* open_file_for_write(enum open_status* open_status, char* fname);

enum close_status close_file(FILE *in);

#endif // !UTIL

