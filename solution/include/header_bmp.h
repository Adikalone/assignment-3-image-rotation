#ifndef IO
#define IO
#include <stdio.h>
#endif

#ifndef BMP
#define BMP
#include "image_struct.h"

struct __attribute__((packed)) bmp_header
{
        uint16_t bfType; //type of file
        uint32_t bfileSize; //size of file
        uint32_t bfReserved; //reserved bits
        uint32_t bOffBits; //offset of start bit
        uint32_t biSize; //headers size = 40 baites
        uint32_t biWidth; //width of picture
        uint32_t biHeight; //height of picture
        uint16_t biPlanes; // == 1
        uint16_t biBitCount; //count colors (bits) in palitra
        uint32_t biCompression; //Compression of file (always 0)
        uint32_t biSizeImage; //size of pictures
        uint32_t biXPelsPerMeter; // horizontal raz
        uint32_t biYPelsPerMeter; // wertical raz
        uint32_t biClrUsed; // count colors used (0 = all)
        uint32_t biClrImportant; //count colors matter (0 = all)
};



struct bmp_header* create_headers(void);

void free_header(struct bmp_header* bmp_header);

enum read_status{
  READ_OK = 0,
  READ_INVALID_SIGNATURE,
  READ_INVALID_BITS,
  READ_INVALID_HEADER,
  READ_INVALID_TYPE
};

enum read_status from_bmp(FILE *in, struct image* img);

enum write_status {
  WRITE_OK = 0,
  WRITE_ERROR
};


enum write_status to_bmp(FILE *out, struct image const* img);

#endif // !BMP
