#ifndef INT_T
#define INT_T
#include <stdint.h>
#endif

#ifndef IMAGE
#define IMAGE

struct image{
  uint64_t width, height;
  struct pixel* data;
};
struct pixel{
  uint8_t b, g, r;
};

struct image* create_empty_image(void);

void get_memmory_for_data(struct image* img, const uint64_t width, const uint64_t height);

struct image* new_image(const uint64_t width, const uint64_t height);

void free_image(struct image* image);

void free_empty_image(struct image* image);

#endif




